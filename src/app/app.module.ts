import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { MeuComponentComponent } from './meu-component/meu-component.component';
import { Component2Component } from './component2/component2.component';
import { Component3Component } from './component3/component3.component';
import { Component4Component } from './component4/component4.component';
import { Component5Component } from './component5/component5.component';
import { AlteraCorDirective } from './altera-cor.directive';
import { NgUnlessDirective } from './ng-unless.directive';

@NgModule({
  declarations: [
    AppComponent,
    MeuComponentComponent,
    Component2Component,
    Component3Component,
    Component4Component,
    Component5Component,
    AlteraCorDirective,
    NgUnlessDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
