import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-component2',
  templateUrl: './component2.component.html',
  styleUrls: ['./component2.component.css']
})
export class Component2Component implements OnInit {

  myStyle = '';

  get mySafeStyle() {
    return this.san.bypassSecurityTrustStyle(this.myStyle);
  }

  constructor(
    private san: DomSanitizer
  ) { }

  ngOnInit() {
  }

}
