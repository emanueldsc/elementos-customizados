import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-component5',
  templateUrl: './component5.component.html',
  styleUrls: ['./component5.component.css']
})
export class Component5Component implements OnInit {

  @ViewChild('comboBox', { static: false }) comboBox: ElementRef;

  constructor() {

  }

  ngAfterViewInit(): void {
  }

  ngOnInit() {
    console.log('onInit')
    console.log(this.comboBox);
  }

  switchColor() {

  }

}
