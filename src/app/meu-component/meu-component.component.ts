import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
// import { NOMEM } from 'dns';

@Component({
  selector: 'app-meu-component',
  templateUrl: './meu-component.component.html',
  styleUrls: ['./meu-component.component.css']
})
export class MeuComponentComponent implements OnInit, OnDestroy {

  nome = 'Emanuel Douglas';

  @ViewChild('meuElemento', { static: false }) myElement: HTMLElement;

  // @Input() nome;
  // @Output() nomeChange = new EventEmitter();

  /**
   * Recebe dados de fora do componenet e recupera internamente.
   */
  @Input('inputParaEnviarDadosParaComponenet') inputExterno;

  @Output('myClick') myClick = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  
  /**
   * Acessar o elemento via ViewChild so é possivel apos ser executado a função afterview, 
   * pois é assegurado qeu o elemento ja esteja renderizado.
   */
  ngAfterViewInit(): void {
    console.log('---###---###---###---###---###---###');
    console.log(this.myElement);
    console.log('---###---###---###---###---###---###');
  }

  ngOnDestroy(): void {
    console.log('tchau!');
  }

  /**
   * Função acionada quando o botão interno do componenet é clicado.
   */
  callClick() {
    /**
     * A variável myClick recebe a função que vem do componenete mais externo
     * e essa função é associado a myClick.
     * "emit(123)" é o valor que vai ser recebido pelo $event no componenet pai 
     * que chamou este componenet.
     */
    this.myClick.emit(123);
    alert(123);
  }

  // onChangeName() {
  //   this.nomeChange.emit(this.nome);
  // }

}
