import { Directive, ElementRef, Renderer2, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appAlteraCor]'
})
export class AlteraCorDirective {

  constructor(
    private elementRef: ElementRef,
    private render: Renderer2
  ) { 
    let color = '';
    switch(Math.floor(Math.random() *4 )){
      case 0: color = 'red'; break;
      case 1: color = 'blue'; break;
      case 2: color = 'green'; break;
      case 3: color = 'orange'; break;
    }
    // this.elementRef.nativeElement.style.backgroundColor = color;
    this.render.setStyle(this.elementRef.nativeElement, 'backgroundColor', color);
  }

  @HostListener('click') handleClick(event) {
    this.minhaCorDeFont = 'white';
  }

  @HostBinding('style.color') minhaCorDeFont: string;



}
